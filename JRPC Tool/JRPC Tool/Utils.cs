﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows;
using JRPC_Client;
using XDevkit;

namespace JRPC_Tool
{
    public class Utils
    {

    public enum XMessageBoxIcons
    {
        XBM_NOICON,
        XMB_ERRORICON,
        XMB_WARNINGICON,
        XMB_ALERTICON
    }

    public class XMessageBoxUIProgress : EventArgs
    {
        public uint Result { get; private set; }
        public uint Code { get; private set; }

        private XMessageBoxUIProgress() { }

        public XMessageBoxUIProgress(uint result, uint code)
        {
            Result = result;
            Code = code;
        }
    }
    public class ActiveXMessageBoxes
    {
        public uint Size;
        public byte[] XOverlappedBytes;
        public ActiveXMessageBoxes(uint size, byte[] xOverlappedBytes)
        {
            Size = size;
            XOverlappedBytes = xOverlappedBytes;
        }
    }
    public static class XMessageBoxTracking
    {
        public static List<ActiveXMessageBoxes> ActiveMessageBoxes = new List<ActiveXMessageBoxes>();
    }
    public class XMessageBoxUI
    {
        public MainWindow Form = Application.Current.Windows[0] as MainWindow;
        public ActiveXMessageBoxes CurMessageBox;
        public volatile uint XOverlappedAddr;
        public volatile uint ResultAddr;
        public volatile bool IsMessageBoxOpen = false;
        public event EventHandler<XMessageBoxUIProgress> MessageBoxUIResult;

        public string MessageBoxTitle;
        public string MessageBoxMessage;
        public string[] XMessageBoxButtons;
        public XMessageBoxIcons XMessageBoxIcon;
        public int SelectedButton = 0;
        public XMessageBoxUI(IXboxConsole console, string MsgBoxTitle, string MsgBoxMsg, string[] MsgBoxButtons, XMessageBoxIcons Icon, int SelectedButtonIndex)
        {
            Form.Console = console;
            MessageBoxTitle = MsgBoxTitle;
            MessageBoxMessage = MsgBoxMsg;
            XMessageBoxButtons = MsgBoxButtons;
            XMessageBoxIcon = Icon;
            SelectedButton = SelectedButtonIndex;
        }

        public void RemoveMessageBox(ActiveXMessageBoxes mBox)
        {
            XMessageBoxTracking.ActiveMessageBoxes.Remove(CurMessageBox);
            if (XMessageBoxTracking.ActiveMessageBoxes.Count() > 0)
                Form.Console.SetMemory(XOverlappedAddr, XMessageBoxTracking.ActiveMessageBoxes.Last().XOverlappedBytes);
        }

        public void CheckMessageBoxResult()
        {
            while (IsMessageBoxOpen)
            {
                try
                {
                    if (CurMessageBox == XMessageBoxTracking.ActiveMessageBoxes.Last())
                    {
                        uint code = Form.Console.ReadUInt32(XOverlappedAddr);
                        uint result = 0;
                        switch (code)
                        {
                            case 0: //the user clicked one of the buttons
                                result = Form.Console.ReadUInt32(ResultAddr);
                                IsMessageBoxOpen = false;
                                break;
                            case 0x65b: //the user pressed b or back to close the message box ui
                            case 0x4c7: //user pressed the guide button to close the message box
                                result = 420;
                                IsMessageBoxOpen = false;
                                break;
                            case 0x3e5: //the messagebox is still open
                                break;
                            default: //unhandled case
                                result = 710;
                                IsMessageBoxOpen = false;
                                break;
                        }
                        if (!IsMessageBoxOpen)
                        {
                            RemoveMessageBox(CurMessageBox);
                            MessageBoxUIResult(this, new XMessageBoxUIProgress(result, code));
                        }
                    }
                    Thread.Sleep(200);
                }
                catch (Exception)
                {
                    return;
                }
            }
        }

        public bool Show()
        {
            if (IsMessageBoxOpen)
            {
                MessageBox.Show("This XMessageBox is already open. Close it before opening it again.", "Error");
                return false;
            }
            if (XMessageBoxButtons.Count() > 3)
            {
                MessageBox.Show("Number of buttons may not exceed 3.", "Error");
                return false;
            }
            else if (XMessageBoxButtons.Count() == 0)
            {
                MessageBox.Show("Must have at least one button.", "Error");
                return false;
            }

            uint freeMemory = Form.Console.ResolveFunction("xam.xex", 2601) + 0x3000;
            
            //XOverlapped and Result share the same address for every message box... no clue how to ger around this.
            uint xOverlapped = freeMemory;
            uint result = freeMemory + 0x20; // xoverlapped is about 0x18 in size... we'll just do 0x20 to be safe

            //If we already have message boxes open, increment the new messageboxes location in memory
            foreach(ActiveXMessageBoxes mb in XMessageBoxTracking.ActiveMessageBoxes)
                freeMemory += mb.Size;

            //setup our title text and copy it into memory on the console
            byte[] title = JRPC.WCHAR(MessageBoxTitle);
            uint Title = result + 0x10;
            Form.Console.SetMemory(Title, title);

            //setup our message text and copy it into memory on the console
            byte[] msg = JRPC.WCHAR(MessageBoxMessage);
            uint Msg = Title + (uint)title.Length;
            Form.Console.SetMemory(Msg, msg);

            //setup our button text and copy it into memory on the console
            List<byte[]> ButtonList = new List<byte[]>();
            foreach (string s in XMessageBoxButtons)
                ButtonList.Add(JRPC.ToWCHAR(s));
            uint ButtonAddr = Msg + (uint)msg.Length;
            uint OrigButtonAddr = ButtonAddr;
            foreach (byte[] b in ButtonList)
            {
                Form.Console.SetMemory(ButtonAddr, b);
                ButtonAddr += (uint)b.Length;
            }

            //copy pointers to button wstrs into an array
            uint EndAddr = freeMemory;
            for (int i = 0; i < ButtonList.Count(); i++)
            {
                EndAddr = ButtonAddr + ((uint)i * 4);
                Form.Console.WriteUInt32(EndAddr, OrigButtonAddr);
                OrigButtonAddr += (uint)ButtonList.ElementAt(i).Length;
            }

            //finally, call XamShowMessageBoxUI
            uint LocalClientIndex = 0;

            uint addr = Form.Console.ResolveFunction("xam.xex", 0x2ca);
            int ret = Form.Console.Call<int>(addr, LocalClientIndex, Title, Msg, ButtonList.Count(), ButtonAddr, SelectedButton, (uint)XMessageBoxIcon, result, xOverlapped);

            if (ret == 0x3e5) //messagebox was opened on the console successfully
            {
                IsMessageBoxOpen = true;

                //set our xoverlapped and result addresses for later use
                XOverlappedAddr = xOverlapped;
                ResultAddr = XOverlappedAddr + 0x20;

                //grab the current xoverlapped bytes from the console and store them in an array incase we popup a messagebox on top of this one
                byte[] XOverlappedBytes = Form.Console.GetMemory(XOverlappedAddr, 0x20);

                //grab the size of all the stuff we just set into memory on the console
                uint Size = (EndAddr + 0x4) - freeMemory;
                while ((Size & 0x3) != 0) //make sure address is 4 byte aligned (don't know if this even matters)
                    Size += 0x1;

                //add this messagebox to the list of active messageboxes
                CurMessageBox = new ActiveXMessageBoxes(Size, XOverlappedBytes);
                XMessageBoxTracking.ActiveMessageBoxes.Add(CurMessageBox);

                //create a new thread to monitor the messagebox
                Thread MessageBoxThread = new Thread(CheckMessageBoxResult);
                MessageBoxThread.Start();
            }
            else
                IsMessageBoxOpen = false;

            return IsMessageBoxOpen;
        }
    }
        public enum XNotiyLogo
        {
            XBOX_LOGO = 0,
            NEW_MESSAGE_LOGO = 1,
            FRIEND_REQUEST_LOGO = 2,
            NEW_MESSAGE = 3,
            FLASHING_XBOX_LOGO = 4,
            GAMERTAG_SENT_YOU_A_MESSAGE = 5,
            GAMERTAG_SINGED_OUT = 6,
            GAMERTAG_SIGNEDIN = 7,
            GAMERTAG_SIGNED_INTO_XBOX_LIVE = 8,
            GAMERTAG_SIGNED_IN_OFFLINE = 9,
            GAMERTAG_WANTS_TO_CHAT = 10, // 0x0000000A
            DISCONNECTED_FROM_XBOX_LIVE = 11, // 0x0000000B
            DOWNLOAD = 12, // 0x0000000C
            FLASHING_MUSIC_SYMBOL = 13, // 0x0000000D
            FLASHING_HAPPY_FACE = 14, // 0x0000000E
            FLASHING_FROWNING_FACE = 15, // 0x0000000F
            FLASHING_DOUBLE_SIDED_HAMMER = 16, // 0x00000010
            GAMERTAG_WANTS_TO_CHAT_2 = 17, // 0x00000011
            PLEASE_REINSERT_MEMORY_UNIT = 18, // 0x00000012
            PLEASE_RECONNECT_CONTROLLERM = 19, // 0x00000013
            GAMERTAG_HAS_JOINED_CHAT = 20, // 0x00000014
            GAMERTAG_HAS_LEFT_CHAT = 21, // 0x00000015
            GAME_INVITE_SENT = 22, // 0x00000016
            FLASH_LOGO = 23, // 0x00000017
            PAGE_SENT_TO = 24, // 0x00000018
            FOUR_2 = 25, // 0x00000019
            FOUR_3 = 26, // 0x0000001A
            ACHIEVEMENT_UNLOCKED = 27, // 0x0000001B
            FOUR_9 = 28, // 0x0000001C
            GAMERTAG_WANTS_TO_TALK_IN_VIDEO_KINECT = 29, // 0x0000001D
            VIDEO_CHAT_INVITE_SENT = 30, // 0x0000001E
            READY_TO_PLAY = 31, // 0x0000001F
            CANT_DOWNLOAD_X = 32, // 0x00000020
            DOWNLOAD_STOPPED_FOR_X = 33, // 0x00000021
            FLASHING_XBOX_CONSOLE = 34, // 0x00000022
            X_SENT_YOU_A_GAME_MESSAGE = 35, // 0x00000023
            DEVICE_FULL = 36, // 0x00000024
            FOUR_7 = 37, // 0x00000025
            FLASHING_CHAT_ICON = 38, // 0x00000026
            ACHIEVEMENTS_UNLOCKED = 39, // 0x00000027
            X_HAS_SENT_YOU_A_NUDGE = 40, // 0x00000028
            MESSENGER_DISCONNECTED = 41, // 0x00000029
            BLANK = 42, // 0x0000002A
            CANT_SIGN_IN_MESSENGER = 43, // 0x0000002B
            MISSED_MESSENGER_CONVERSATION = 44, // 0x0000002C
            FAMILY_TIMER_X_TIME_REMAINING = 45, // 0x0000002D
            DISCONNECTED_XBOX_LIVE_11_MINUTES_REMAINING = 46, // 0x0000002E
            KINECT_HEALTH_EFFECTS = 47, // 0x0000002F
            FOUR_5 = 48, // 0x00000030
            GAMERTAG_WANTS_YOU_TO_JOIN_AN_XBOX_LIVE_PARTY = 49, // 0x00000031
            PARTY_INVITE_SENT = 50, // 0x00000032
            GAME_INVITE_SENT_TO_XBOX_LIVE_PARTY = 51, // 0x00000033
            KICKED_FROM_XBOX_LIVE_PARTY = 52, // 0x00000034
            NULLED = 53, // 0x00000035
            DISCONNECTED_XBOX_LIVE_PARTY = 54, // 0x00000036
            DOWNLOADED = 55, // 0x00000037
            CANT_CONNECT_XBL_PARTY = 56, // 0x00000038
            GAMERTAG_HAS_JOINED_XBL_PARTY = 57, // 0x00000039
            GAMERTAG_HAS_LEFT_XBL_PARTY = 58, // 0x0000003A
            GAMER_PICTURE_UNLOCKED = 59, // 0x0000003B
            AVATAR_AWARD_UNLOCKED = 60, // 0x0000003C
            JOINED_XBL_PARTY = 61, // 0x0000003D
            PLEASE_REINSERT_USB_STORAGE_DEVICE = 62, // 0x0000003E
            PLAYER_MUTED = 63, // 0x0000003F
            PLAYER_UNMUTED = 64, // 0x00000040
            FLASHING_CHAT_SYMBOL = 65, // 0x00000041
            UPDATING = 76, // 0x0000004C
        }
    }
}
