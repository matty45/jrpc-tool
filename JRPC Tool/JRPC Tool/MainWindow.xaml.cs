﻿using System;
using System.Windows;
using JRPC_Client;
using XDevkit;

namespace JRPC_Tool
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public IXboxConsole Console;

        public MainWindow()
        {
            InitializeComponent();
            XNotifyComboBox.ItemsSource = Enum.GetValues(typeof(Utils.XNotiyLogo));
            XMessageBoxComboBox.ItemsSource = Enum.GetValues((typeof(Utils.XMessageBoxIcons)));
            TopLeftLight.ItemsSource = Enum.GetValues(typeof(JRPC.LEDState));
            TopRightLight.ItemsSource = Enum.GetValues(typeof(JRPC.LEDState));
            BottomLeftLight.ItemsSource = Enum.GetValues(typeof(JRPC.LEDState));
            BottomRightLight.ItemsSource = Enum.GetValues(typeof(JRPC.LEDState));
            UnlockButtons(false);
        }

        public void UnlockButtons(bool cond)
        {
            MainTabControl.IsEnabled = cond;
        }

        public void GrabInfo()
        {
            XInfoIpBox.Text = Console.XboxIP();
            XInfoNameBox.Text = Console.Name;
        }

        private void ConnectButton_Click(object sender, RoutedEventArgs e)
        {
            if (Console.Connect(out Console, IpBox.Text))
            { 
                ConnectButton.Content = "Re-connect";
                UnlockButtons(true);
                GrabInfo();
                MessageBox.Show("Successfully connected to your console.", "Hooray!");
            }
            else
            {
                UnlockButtons(false);
                MessageBox.Show("Did not manage to connect to your console, please check if the JRPC2 plugin is setup inside dashlaunch and that your console is in the same network as you. Make sure to type the correct ip address or name of your console.", "Error");
            }
        }

        private void XNotifyButton_Click(object sender, RoutedEventArgs e)
        {

            uint logo = Convert.ToUInt16(XNotifyComboBox.SelectedValue);
            Console.XNotify(XNotifyTextBox.Text,logo);
        }

        private void XMessageBoxButton_Click(object sender, RoutedEventArgs e)
        {
            Utils.XMessageBoxIcons icon = (Utils.XMessageBoxIcons) XMessageBoxComboBox.SelectedValue;
            Utils.XMessageBoxUI message = new Utils.XMessageBoxUI(Console, XMessageBoxCaptionTextBox.Text, XMessageBoxBodyTextBox.Text, new[] { "Yes", "No" }, icon, 0);
            message.Show();
        }

        private void ChangeXboxLightsButton_Click(object sender, RoutedEventArgs e)
        {
            Console.SetLeds((JRPC.LEDState) TopLeftLight.SelectedValue, (JRPC.LEDState)TopRightLight.SelectedValue, (JRPC.LEDState)BottomLeftLight.SelectedValue, (JRPC.LEDState)BottomRightLight.SelectedValue);
        }

        private void ShutdownXboxButton_Click(object sender, RoutedEventArgs e)
        {
            Console.ShutDownConsole();
            UnlockButtons(false);
        }

        private void ColdRebootXboxButton_Click(object sender, RoutedEventArgs e)
        {
            Console.Reboot(null, null, null, XboxRebootFlags.Cold);
            UnlockButtons(false);
        }

        private void WarmRebootXboxButton_Click(object sender, RoutedEventArgs e)
        {
            Console.Reboot(null, null, null, XboxRebootFlags.Warm);
            UnlockButtons(false);
        }
    }
}
